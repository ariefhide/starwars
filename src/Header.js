import React, { Component } from 'react';
import 'materialize-css/dist/css/materialize.min.css';

class Header extends Component {
  constructor(props) {

    super(props);

    this.state = {
      nama: 'masukan nama'
    };
  }


  render() {
    return (
      <div>
        <div class="row">
          <div class="input-field col l2">
            <i class="material-icons prefix">account_circle</i>
            <input id="last_name" type="text" class="validate" />
            <label for="last_name">{this.state.nama}</label>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;