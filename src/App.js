import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import './App.css';

import LandingPage from './views/Pages/LandingPage';
import Character from './views/Pages/Character';
import Error from "./views/Pages/Error";
import Starship from './views/Pages/Starship';
import Vehicles from './views/Pages/Vehicles';
import Films from './views/Pages/Films';
import Species from './views/Pages/Species';

import { BrowserRouter, Route, Switch } from "react-router-dom";


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
        bg: 'bg.jpg'
    }
  }
  render() {
    return (
      <BrowserRouter>
        <div className="page" style={ { backgroundImage: `url(img/${this.state.bg})` } }>
          <LandingPage/>
          <Switch>
            <Route path="/" component={Character} exact />
            <Route path="/Character" component={Character} />
            <Route path="/Starship" component={Starship} />
            <Route path="/Vehicles" component={Vehicles}/>
            <Route path="/Films" component={Films}/>
            <Route path="/Species" component={Species}/>
            <Route component={Error} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;