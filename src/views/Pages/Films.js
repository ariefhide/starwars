import React, { Component } from 'react';
import Axios from 'axios';

function searchingFor(imageShow) {
  return function (x) {
    return x.title.toLowerCase().includes(imageShow.toLowerCase()) || false;
  }
}

class Films extends Component {
  constructor(props) {
    super(props);
    this.state = {
      films: [],
      modalFilm: false,
      imageShow: ''
    }
  }

  componentDidMount() {
    Axios.get(`https://swapi.co/api/films/`)
      .then(res => {
        this.setState({ films: res.data.results });
      });
  }
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handlemodalFilm = (e) => {
    this.setState({ modalFilm: true, imageShow: e.target.name })
  }


  render() {
    const { films, modalFilm, imageShow } = this.state
    return (
      <div style={{ marginTop: 100 }}>

        <div className={`modal ${modalFilm && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.toggle('modalFilm')}></div>
          {
            films.filter(searchingFor(imageShow)).map(film => (
              <div className="modal-card" key={film.title} style={{width:'70%'}}>
                <header className="modal-card-head " style={{ background: '#0e2ea0' }}>
                  <span className="modal-card-title" style={{ color: 'white', marginLeft: 170, fontSize: 30 }}>Detail film</span>
                  <button
                    className="delete"
                    aria-label="close"
                    onClick={() => this.toggle('modalFilm')}
                  />
                </header>
                <section className="modal-card-body">
                  <div className="columns">
                    <div className="column is-6">
                      <figure className="image is-1by1">
                        <img className="is-rounded" src={`img/films/${film.title}.jpg`} />
                      </figure>
                    </div>
                    <div className="column is-6">
                      <table>
                        <tbody>
                          <tr>
                            <td>Title</td>
                            <td>{film.title}</td>
                          </tr>
                          <tr>
                            <td>Episode</td>
                            <td>{film.episode_id}</td>
                          </tr>
                          <tr>
                            <td>Opening crawl</td>
                            <td>{film.opening_crawl}</td>
                          </tr>
                          <tr>
                            <td>Director</td>
                            <td>{film.director}</td>
                          </tr>
                          <tr>
                            <td>Producer</td>
                            <td>{film.producer}</td>
                          </tr>
                          <tr>
                            <td>Release</td>
                            <td>{film.release_date}</td>
                          </tr>
                          <tr>
                            <td>Character</td>
                            <td>{film.characters}</td>
                          </tr>
                          <tr>
                            <td>Planets</td>
                            <td>{film.planets}</td>
                          </tr>
                          <tr>
                            <td>Starships</td>
                            <td>{film.starships}</td>
                          </tr>
                          <tr>
                            <td>Vehicles</td>
                            <td>{film.vehicles}</td>
                          </tr>
                          <tr>
                            <td>Species</td>
                            <td>{film.species}</td>
                          </tr>
                          <tr>
                            <td>Created</td>
                            <td>{film.created}</td>
                          </tr>
                          <tr>
                            <td>Edited</td>
                            <td>{film.edited}</td>
                          </tr>
                          <tr>
                            <td>Url</td>
                            <td>{film.url}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            ))}
        </div>

        <h1 className="has-text-centered" style={{ color: 'white', fontSize: 30, marginBottom: 100 }}>films List</h1>
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="columns is-multiline">
              {
                films.map(film => (
                  <div className="column is-3" key={film.title}>
                    <div className="card" >
                      <div className="card-image pesan">
                        <figure className="image is-1by1">
                          <img
                            className="is-square"
                            name={film.title}
                            src={`img/films/${film.title}.jpg`}
                            onClick={(e) => this.handlemodalFilm(e)}
                          />
                        </figure>
                      </div>
                      <div className="card-content has-text-centered">
                        <b>
                          {film.title}
                        </b>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Films;
