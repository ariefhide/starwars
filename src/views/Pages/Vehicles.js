import React, { Component } from 'react';
import Axios from 'axios';

function searchingFor(imageShow) {
  return function (x) {
    return x.name.toLowerCase().includes(imageShow.toLowerCase()) || false;
  }
}

class Vehicles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vehicles: [],
      modalVehicle: false,
      imageShow: ''
    }
  }

  componentDidMount() {
    Axios.get(`https://swapi.co/api/vehicles/`)
      .then(res => {
        console.log(res, 'ini res vehicles');
        this.setState({ vehicles: res.data.results });
      });
  }
  
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handlemodalVehicle = (e, item) => {
    this.setState({ modalVehicle: true, imageShow: e.target.name })
  }

  render() {
    const { vehicles, modalVehicle, imageShow, item } = this.state
    return (
      <div style={{marginTop:100}}>

        <div className={`modal ${modalVehicle && 'is-active'}`} >
          <div className="modal-background" onClick={() => this.toggle('modalVehicle')}></div>
          {
            vehicles.filter(searchingFor(imageShow)).map(vehicle => (
              <div className="modal-card" key={vehicle.name}>
                <header className="modal-card-head " style={{background:'#0e2ea0'}}>
                  <span className="modal-card-title" style={{color:'white',marginLeft:170,fontSize:30}}>Detail Character</span>
                  <button
                    className="delete"
                    aria-label="close"
                    onClick={() => this.toggle('modalVehicle')}
                  />
                </header>
                <section className="modal-card-body">
                  <div className="columns">
                    <div className="column is-6">
                      <figure className="image is-1by1">
                        <img className="is-rounded" src={`img/vehicles/${vehicle.name}.jpg`} />
                      </figure>
                    </div>
                    <div className="column is-6">
                      <table>
                        <tbody>
                          <tr>
                            <td>Name</td>
                            <td>{vehicle.name}</td>
                          </tr>
                          <tr>
                            <td>Model</td>
                            <td>{vehicle.model}</td>
                          </tr>
                          <tr>
                            <td>Manufacturer</td>
                            <td>{vehicle.manufacturer}</td>
                          </tr>
                          <tr>
                            <td>vehicle on cost</td>
                            <td>{vehicle.cost_in_credits}</td>
                          </tr>
                          <tr>
                            <td>Length</td>
                            <td>{vehicle.length}</td>
                          </tr>
                          <tr>
                            <td>Max atmosphering speed</td>
                            <td>{vehicle.max_atmosphering_speed}</td>
                          </tr>
                          <tr>
                            <td>vehicle crew</td>
                            <td>{vehicle.crew}</td>
                          </tr>
                          <tr>
                            <td>vehicle passangers</td>
                            <td>{vehicle.passengers}</td>
                          </tr>
                          <tr>
                            <td>vehicle cargo</td>
                            <td>{vehicle.cargo_capacity}</td>
                          </tr>
                          <tr>
                            <td>vehicle consumables</td>
                            <td>{vehicle.consumables}</td>
                          </tr>
                          <tr>
                            <td>vehicle Class</td>
                            <td>{vehicle.vehicle_class}</td>
                          </tr>
                          <tr>
                            <td>vehicle Pilots</td>
                            <td>{vehicle.pilots}</td>
                          </tr>
                          <tr>
                            <td>vehicle Films</td>
                            <td>{vehicle.films}</td>
                          </tr>
                          <tr>
                            <td>vehicle Created</td>
                            <td>{vehicle.created}</td>
                          </tr>
                          <tr>
                            <td>vehicle edited</td>
                            <td>{vehicle.edited}</td>
                          </tr>
                          <tr>
                            <td>vehicle URL</td>
                            <td>{vehicle.url}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            ))}
        </div>

        <h1 className="has-text-centered" style={{color:'white',fontSize:30,marginBottom:100}}>vehicles List</h1>
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="columns is-multiline">
              {
                vehicles.map(vehicle => (
                  <div className="column is-3" key={vehicle.name}>
                    <div className="card" >
                      <div className="card-image pesan">
                        <figure className="image is-1by1">
                          <img
                            className="is-square"
                            name={vehicle.name}
                            src={`img/vehicles/${vehicle.name}.jpg`}
                            alt="invalid name"
                            onClick={(e) => this.handlemodalVehicle(e)}
                          />
                        </figure>
                      </div>
                      <div className="card-content has-text-centered">
                        <b>
                          {vehicle.name}
                        </b>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Vehicles;
