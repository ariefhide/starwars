import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Link } from 'react-router-dom';
import './LandingPage.css';


class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navbar: false
    };
  }
  handleNavbar = () => {
    if (this.state.navbar === false) {
      this.setState({ navbar: true })
    } else {
      this.setState({ navbar: false })
    }
  }
  render() {
    const { navbar } = this.state
    return (
      <div>
        <img src="/img/head.jpg" style={{width:'100%', height: 700, marginTop: -100 }} />
        <section>
          <nav className="navbar" role="navigation" aria-label="main navigation" style={{ background: '#222',marginTop:-50 }}>
            <div className="navbar-brand" style={{ marginLeft: 320 }}>
              <a role="button" onClick={this.handleNavbar} className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </a>
            </div>
            <div id="navbarBasicExample" className={`navbar-menu ${navbar && 'is-active has-text-info has-text-centered'}`} ref="toggle">
              <div className="navbar-start" >
                <Link to="/Character">
                  <div className="navbar-item" style={{height:'100%'}}>
                    <img src="img/icon-char.png"/>&nbsp;
                    <p style={{ color: 'white' }}>Character</p>
                  </div>
                </Link>
                <Link to="/Starship">
                  <div className="navbar-item" style={{height:'100%'}}>
                    <img src="img/icon-space.png"/>&nbsp;
                    <p style={{ color: 'white' }}>Spaceship</p>
                  </div>
                </Link>
                <Link to="/Vehicles">
                  <div className="navbar-item" style={{height:'100%'}}>
                    <img src="img/icon-vehicles.png"/>&nbsp;
                    <p style={{ color: 'white' }}>Vehicles</p>
                  </div>
                </Link>
                <Link to="/Films">
                  <div className="navbar-item" style={{height:'100%',paddingLeft:30}}>
                  <img src="img/icon-film.png"/>&nbsp;
                    <p style={{ color: 'white' }}>Films</p>
                  </div>
                </Link>
                <Link to="/Species">
                  <div className="navbar-item" style={{height:'100%',paddingLeft:30}}>
                    <img src="img/icon-species.png" />&nbsp;
                    <p style={{ color: 'white' }}>species</p>
                  </div>
                </Link>
              </div>
            </div>
          </nav>
        </section>
      </div>
        );
      }
    }
    
    export default Header;
