import React, { Component } from 'react';
import Axios from 'axios';

function searchingFor(imageShow) {
  return function (x) {
    return x.name.toLowerCase().includes(imageShow.toLowerCase()) || false;
  }
}

class Starship extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ships: [],
      modalShip: false,
      imageShow: ''
    }
  }

  componentDidMount() {
    Axios.get(`https://swapi.co/api/starships/`)
      .then(res => {
        this.setState({ ships: res.data.results });
      });
  }

  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handlemodalShip = (e) => {
    this.setState({ modalShip: true, imageShow: e.target.name })
  }

  render() {
    const { ships, modalShip, imageShow } = this.state
    return (
      <div style={{ marginTop: 100 }}>
        <div className={`modal ${modalShip && 'is-active'}`} >
          <div className="modal-background" onClick={() => this.toggle('modalShip')}></div>
          {
            ships.filter(searchingFor(imageShow)).map(ship => (
              <div className="modal-card" key={ship.name}>
                <header className="modal-card-head " style={{ background: '#0e2ea0' }}>
                  <span className="modal-card-title" style={{ color: 'white', marginLeft: 170, fontSize: 30 }}>Detail Ship</span>
                  <button
                    className="delete"
                    aria-label="close"
                    onClick={() => this.toggle('modalShip')}
                  />
                </header>
                <section className="modal-card-body">
                  <div className="columns">
                    <div className="column is-6">
                      <figure className="image is-1by1">
                        <img className="is-rounded" src={`img/ship/${ship.name}.jpg`} />
                      </figure>
                    </div>
                    <div className="column is-6">
                      <table>
                        <tbody>
                          <tr>
                            <td>Name</td>
                            <td>{ship.name}</td>
                          </tr>
                          <tr>
                            <td>Model</td>
                            <td>{ship.model}</td>
                          </tr>
                          <tr>
                            <td>Manufacturer</td>
                            <td>{ship.manufacturer}</td>
                          </tr>
                          <tr>
                            <td>Ship on cost</td>
                            <td>{ship.cost_in_credits}</td>
                          </tr>
                          <tr>
                            <td>Length</td>
                            <td>{ship.length}</td>
                          </tr>
                          <tr>
                            <td>Max atmosphering speed</td>
                            <td>{ship.max_atmosphering_speed}</td>
                          </tr>
                          <tr>
                            <td>Ship crew</td>
                            <td>{ship.crew}</td>
                          </tr>
                          <tr>
                            <td>Ship passangers</td>
                            <td>{ship.passengers}</td>
                          </tr>
                          <tr>
                            <td>Ship cargo</td>
                            <td>{ship.cargo_capacity}</td>
                          </tr>
                          <tr>
                            <td>Ship consumables</td>
                            <td>{ship.consumables}</td>
                          </tr>
                          <tr>
                            <td>Ship hyperdrive</td>
                            <td>{ship.hyperdrive_rating}</td>
                          </tr>
                          <tr>
                            <td>Ship MGLT</td>
                            <td>{ship.MGLT}</td>
                          </tr>
                          <tr>
                            <td>Ship Class</td>
                            <td>{ship.starship_class}</td>
                          </tr>
                          <tr>
                            <td>Ship Pilots</td>
                            <td>{ship.pilots}</td>
                          </tr>
                          <tr>
                            <td>Ship Films</td>
                            <td>{ship.films}</td>
                          </tr>
                          <tr>
                            <td>Ship Created</td>
                            <td>{ship.created}</td>
                          </tr>
                          <tr>
                            <td>Ship edited</td>
                            <td>{ship.edited}</td>
                          </tr>
                          <tr>
                            <td>Shipe URL</td>
                            <td>{ship.url}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            ))}
        </div>

        <h1 className="has-text-centered" style={{ color: 'white', fontSize: 30, marginBottom: 100 }}>Starship List</h1>
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="columns is-multiline">
              {
                ships.map(ship => (
                  <div className="column is-3" key={ship.name}>
                    <div className="card" >
                      <div className="card-image pesan">
                        <figure className="image is-1by1">
                          <img
                            className="is-square"
                            name={ship.name}
                            src={`img/ship/${ship.name}.jpg`}
                            onClick={(e) => this.handlemodalShip(e)}
                          />
                        </figure>
                      </div>
                      <div className="card-content has-text-centered">
                        <b>
                          {ship.name}
                        </b>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Starship;
