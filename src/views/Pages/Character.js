import React, { Component } from 'react';
import Axios from 'axios';
import './Character.css';

function searchingFor(imageShow) {
  return function (x) {
    return x.name.toLowerCase().includes(imageShow.toLowerCase()) || false;
  }
}

class Character extends Component {
  constructor(props) {
    super(props);
    this.state = {
      peoples: [],
      modalChar: false,
      imageShow: ''

    }
  }

  componentDidMount() {
    Axios.get(`https://swapi.co/api/people/`)
      .then(res => {
        this.setState({ peoples: res.data.results });
      });
  }
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handleModalChar = (e) => {
    this.setState({ modalChar: true, imageShow: e.target.name })
  }

  render() {
    const { peoples, modalChar, imageShow } = this.state
    return (
      <div style={{ marginTop: 100 }}>

        <div className={`modal ${modalChar && 'is-active'}`} >
          <div className="modal-background" onClick={() => this.toggle('modalChar')}></div>
          {
            peoples.filter(searchingFor(imageShow)).map(people => (
              <div className="modal-card" key={people.name} >
                <header className="modal-card-head " style={{background:'#0e2ea0'}}>
                  <span className="modal-card-title" style={{color:'white',marginLeft:170,fontSize:30}}>Detail Character</span>
                  <button
                    className="delete"
                    aria-label="close"
                    onClick={() => this.toggle('modalChar')}
                  />
                </header>
                <section className="modal-card-body">
                  <div className="columns">
                    <div className="column is-6">
                      <figure className="image is-1by1">
                        <img className="is-rounded" src={`img/char/${people.name}.jpg`} />
                      </figure>
                    </div>
                    <div className="column is-6">
                      <table classname="table" >
                        <tbody>
                          <tr>
                            <td>Name</td>
                            <td>{people.name}</td>
                          </tr>
                          <tr>
                            <td>Gender</td>
                            <td>{people.gender}</td>
                          </tr>
                          <tr>
                            <td>Height</td>
                            <td>{people.height}</td>
                          </tr>
                          <tr>
                            <td>Mass</td>
                            <td>{people.mass}</td>
                          </tr>
                          <tr>
                            <td>Hair Color</td>
                            <td>{people.hair_color}</td>
                          </tr>
                          <tr>
                            <td>Skin Color</td>
                            <td>{people.skin_color}</td>
                          </tr>
                          <tr>
                            <td>Eye Color</td>
                            <td>{people.eye_color}</td>
                          </tr>
                          <tr>
                            <td>Birth Year</td>
                            <td>{people.birth_year}</td>
                          </tr>
                          <tr>
                            <td>Homeworld</td>
                            <td>{people.homeworld}</td>
                          </tr>
                          <tr >
                            <td>Films</td>
                            <td>{people.films}</td>
                          </tr>
                          <tr>
                            <td>Species</td>
                            <td>{people.species}</td>
                          </tr>
                          <tr>
                            <td>Vehicles</td>
                            <td>{people.vehicles}</td>
                          </tr>
                          <tr>
                            <td>Starship</td>
                            <td>{people.starships}</td>
                          </tr>
                          <tr>
                            <td>Created</td>
                            <td>{people.created}</td>
                          </tr>
                          <tr>
                            <td>Edited</td>
                            <td>{people.edited}</td>
                          </tr>
                          <tr>
                            <td>URL</td>
                            <td>{people.url}</td>
                          </tr>
                        </tbody>  
                      </table>  
                    </div>
                  </div>
                </section>
              </div>
            ))}
        </div>

            <h1 className="has-text-centered" style={{color:'white',fontSize:30,marginBottom:100}}>Character List</h1>
          
        
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="columns is-multiline">
              {
                peoples.map(people => (
                  <div className="column is-3" key={people.name}>
                    <div className="card" >
                      <div className="card-image pesan">
                        <figure className="image is-1by1">
                          <img
                            className="is-square"
                            name={people.name}
                            src={`img/char/${people.name}.jpg`}
                            onClick={(e) => this.handleModalChar(e)}
                          />
                        </figure>
                      </div>
                      <div className="card-content has-text-centered">
                        <b>
                          {people.name}
                        </b>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Character;
