import React, { Component } from 'react';
import Axios from 'axios';
import './Species.css';

function searchingFor(imageShow) {
  return function (x) {
    return x.name.toLowerCase().includes(imageShow.toLowerCase()) || false;
  }
}

class Species extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Species: [],
      modalSpecies: false,
      imageShow: ''
    }
  }

  componentDidMount() {
    Axios.get(`https://swapi.co/api/species/`)
      .then(res => {
        this.setState({ Species: res.data.results });
      });
  }
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handlemodalSpecies = (e) => {
    this.setState({ modalSpecies: true, imageShow: e.target.name })
  }

  render() {
    const { Species,modalSpecies, imageShow } = this.state
    return (
      <div style={{marginTop:100}}>

       <div className={`modal ${modalSpecies && 'is-active'}`} >
          <div className="modal-background" onClick={() => this.toggle('modalSpecies')}></div>
          {
            Species.filter(searchingFor(imageShow)).map(specie => (
              <div className="modal-card" key={specie.name}>
                <header className="modal-card-head " style={{background:'#0e2ea0'}}>
                  <span className="modal-card-title" style={{color:'white',marginLeft:170,fontSize:30}}>Detail Character</span>
                  <button
                    className="delete"
                    aria-label="close"
                    onClick={() => this.toggle('modalSpecies')}
                  />
                </header>
                <section className="modal-card-body">
                  <div className="columns">
                    <div className="column is-6">
                      <figure className="image is-1by1">
                        <img className="is-rounded" src={`img/species/${specie.name}.jpg`} />
                      </figure>
                    </div>
                    <div className="column is-6">
                      <table classname="table">
                        <tbody>
                          <tr>
                            <td>Name</td>
                            <td>{specie.name}</td>
                          </tr>
                          <tr>
                            <td style={{width:100}}>Classification</td>
                            <td>{specie.classification}</td>
                          </tr>
                          <tr>
                            <td>Designation</td>
                            <td>{specie.designation}</td>
                          </tr>
                          <tr>
                            <td>Average height</td>
                            <td>{specie.average_height}</td>
                          </tr>
                          <tr>
                            <td>Skin colors</td>
                            <td>{specie.skin_colors}</td>
                          </tr>
                          <tr>
                            <td>Hair colors</td>
                            <td>{specie.hair_colors}</td>
                          </tr>
                          <tr>
                            <td>Eye Color</td>
                            <td>{specie.eye_color}</td>
                          </tr>
                          <tr>
                            <td>Average Lifespan</td>
                            <td>{specie.average_lifespan}</td>
                          </tr>
                          <tr>
                            <td>Homeworld</td>
                            <td>{specie.homeworld}</td>
                          </tr>
                          <tr>
                            <td>Language</td>
                            <td>{specie.language}</td>
                          </tr>
                          <tr>
                            <td>People</td>
                            <td>{specie.people}</td>
                          </tr>
                          <tr>
                            <td>Films</td>
                            <td>{specie.films}</td>
                          </tr>
                          <tr>
                            <td>Created</td>
                            <td>{specie.created}</td>
                          </tr>
                          <tr>
                            <td>Edited</td>
                            <td>{specie.edited}</td>
                          </tr>
                          <tr>
                            <td>URL</td>
                            <td>{specie.url}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            ))}
        </div>

      <h1 className="has-text-centered" style={{color:'white',fontSize:30,marginBottom:100}}>species List</h1>
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="columns is-multiline">
              {
                Species.map(specie => (
                  <div className="column is-3" key={specie.name}>
                    <div className="card" >
                      <div className="card-image pesan">
                        <figure className="image is-1by1">
                          <img
                            className="is-square"
                            name={specie.name}
                            src={`img/Species/${specie.name}.jpg`}
                            onClick={(e) => this.handlemodalSpecies(e)}
                          />
                        </figure>
                      </div>
                      <div className="card-content has-text-centered">
                        <b>
                          {specie.name}
                        </b>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Species;
